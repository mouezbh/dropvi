using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Java.Util;
//using Java.Lang;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Hardware;
using Android.Util;
using System.Json;
using System.Threading;
using Android.Locations;
using Wikitude.Architect;
using System.Threading.Tasks;
using Android.Nfc;

namespace Com.Wikitude.Samples
{
	[Activity (Label = "BasicArchitectActivity")]
    public class  BasicArchitectActivity : Activity, ISensorEventListener,ArchitectView.ISensorAccuracyChangeListener, ILocationListener
    {
        bool isLoading = false;
        protected JsonArray poiData;
        IWindowManager windowManager = Android.App.Application.Context.GetSystemService(Context.WindowService).JavaCast<IWindowManager>();

        private SensorManager mSensorManager = null;
        ISensorEventListener sensorlistener;
        private Sensor mGravity;
        private Sensor mAccelerometer;
        private Sensor mMagnetometer;
        bool haveGravity = false;
        bool haveAccelerometer = false;
        bool haveMagnetometer = false;
        private float mAzimuth = float.NaN;
        private List<float[]> mRotHist = new List<float[]>();
        private int mRotHistIndex;
        private int mHistoryMaxLength = 40;
        public static  float TWENTY_FIVE_DEGREE_IN_RADIAN = 0.436332313f;
        public static  float ONE_FIFTY_FIVE_DEGREE_IN_RADIAN = 2.7052603f;
        float[] gData1 = new float[3];
        float[] gData = new float[3]; // gravity or accelerometer
        float[] mData1 = new float[3];
        float[] mData = new float[3]; // magnetometer
        float[] rMat = new float[9];
        float[] iMat = new float[9];
        float[] orientation = new float[3];
        private float inclination;
        public const string EXTRAS_KEY_ACTIVITY_TITLE_STRING = "activityTitle";
		public const string EXTRAS_KEY_ACTIVITY_ARCHITECT_WORLD_URL = "activityArchitectWorldUrl";
        private const string SAMPLE_WORLD_URL = "samples/4_Obtain$Poi$Data_1_From$Application$Model/index.html";
        protected ArchitectView architectView;

		protected Location lastKnownLocation;

		protected ILocationProvider locationProvider;
        

        protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			this.VolumeControlStream = Android.Media.Stream.Music;

			SetContentView(Resource.Layout.sample_cam);

			var title = "Test World";

			if (Intent.Extras != null && Intent.Extras.Get (EXTRAS_KEY_ACTIVITY_TITLE_STRING) != null)
				title = Intent.Extras.GetString (EXTRAS_KEY_ACTIVITY_TITLE_STRING);
				
			Title = title;

			architectView = FindViewById<ArchitectView>(Resource.Id.architectView);

            //TODO: SDK KEY
  //          	var config = new ArchitectView.ArchitectConfig (Constants.WIKITUDE_SDK_KEY);

//            architectView.OnCreate (config);
            StartupConfiguration startupConfiguration = new StartupConfiguration(Constants.WIKITUDE_SDK_KEY, StartupConfiguration.Features.Geo);
           architectView.OnCreate(startupConfiguration);
                this.architectView.RegisterSensorAccuracyChangeListener (this);

			this.locationProvider = new LocationProvider (this, this);
            mSensorManager = (SensorManager)ApplicationContext.GetSystemService(Context.SensorService);
            this.mGravity = this.mSensorManager.GetDefaultSensor(SensorType.Gravity);
            this.haveGravity = this.mSensorManager.RegisterListener(this, this.mGravity, SensorDelay.Normal);
            this.mAccelerometer = this.mSensorManager.GetDefaultSensor(SensorType.Accelerometer);
            this.haveAccelerometer = this.mSensorManager.RegisterListener(this, this.mAccelerometer, SensorDelay.Normal);
            this.mMagnetometer = this.mSensorManager.GetDefaultSensor(SensorType.MagneticField);
            this.haveMagnetometer = this.mSensorManager.RegisterListener(this, this.mMagnetometer, SensorDelay.Normal);
            
        }
        #region ISensorEventListener implementation
        public void OnAccuracyChanged(Sensor sensor, [GeneratedEnum] SensorStatus accuracy)
        {

        }

        public void OnSensorChanged(SensorEvent e)
        {
            float[] data;
            float alpha = 0.97f;

            var ahna = e.Sensor.Type;
            switch (ahna)
            {
                case SensorType.Gravity:
                    for (int i = 0; i < 3; i++)
                    {
                        gData1[i] = e.Values[i];
                    }
               
                    break;
                case SensorType.Accelerometer:
                    for (int i = 0; i < 3; i++)
                    {
                        gData1[i] = e.Values[i];
                       
                    }
                   
                    break;
                case SensorType.MagneticField:
                    for (int i = 0; i < 3; i++)
                    {
                        mData1[i] =e.Values[i];
                    }
                    
                   
                    break;
                default: return;
            }
            var rotation = windowManager.DefaultDisplay.Rotation;
         
            if (SensorManager.GetRotationMatrix(rMat, null, gData1, mData1))
            {
               inclination = (float)Math.Acos(rMat[8]);
                
               // Console.WriteLine(inclination);
                if (inclination < TWENTY_FIVE_DEGREE_IN_RADIAN
                        || inclination > ONE_FIFTY_FIVE_DEGREE_IN_RADIAN)
                {
                    // mFacing is undefined, so we need to clear the history
                    clearRotHist();
                    mAzimuth = float.NaN;
                }
                else
                {
                    setRotHist();
                    // mFacing = azimuth is in radian
                    mAzimuth = (int)(findFacing()*180 / Math.PI+360) % 360 ;
                }
            }
        }
        #endregion
        #region ISensorAccuracyChangeListener implementation
        public void OnCompassAccuracyChanged (int accuracy)
		{
			/* UNRELIABLE = 0, LOW = 1, MEDIUM = 2, Height = 3 */
			if (accuracy < 2 && !this.IsFinishing) 
				Toast.MakeText(this, Resource.String.compass_accuracy_low, ToastLength.Long).Show();
		}
		#endregion

		#region ILocationListener implementation

		public void OnLocationChanged (Location location)
		{
			if (location != null)
				lastKnownLocation = location;

			if (location.HasAltitude)
				architectView.SetLocation (location.Latitude, location.Longitude, location.Altitude, location.HasAccuracy ? location.Accuracy : 1000);
			else
				architectView.SetLocation(location.Latitude, location.Longitude,location.Altitude,location.HasAccuracy ? location.Accuracy : 1000);
		}

		public void OnProviderDisabled (string provider)
		{
		}

		public void OnProviderEnabled (string provider)
		{
		}

		public void OnStatusChanged (string provider, Availability status, Bundle extras)
		{
		}
		#endregion

		protected override void OnResume ()
		{
			base.OnResume ();
            this.mAccelerometer = this.mSensorManager.GetDefaultSensor(SensorType.Accelerometer);
            this.haveAccelerometer = this.mSensorManager.RegisterListener(this, this.mAccelerometer, SensorDelay.Game);

            this.mMagnetometer = this.mSensorManager.GetDefaultSensor(SensorType.MagneticField);
            this.haveMagnetometer = this.mSensorManager.RegisterListener(this, this.mMagnetometer, SensorDelay.Game);

            if (architectView != null)
				architectView.OnResume ();

			if (locationProvider != null)
				locationProvider.OnResume ();
		}

		protected override void OnPause ()
		{
			base.OnPause ();
            this.mSensorManager.UnregisterListener(this);

            if (architectView != null)
				architectView.OnPause ();

			if (locationProvider != null)
				locationProvider.OnPause ();
		}

		protected override void OnStop ()
		{
			base.OnStop ();
		}

		protected override void OnDestroy ()
		{
			base.OnDestroy ();
            this.mSensorManager.UnregisterListener(this);
            if (architectView != null)
			{
				architectView.UnregisterSensorAccuracyChangeListener (this);

				architectView.OnDestroy ();
			}
		}

		public override void OnLowMemory ()
		{
			base.OnLowMemory ();

			if (architectView != null)
				architectView.OnLowMemory ();
		}

		protected override void OnPostCreate (Bundle savedInstanceState)
		{
			base.OnPostCreate (savedInstanceState);
            LoadData();
            if (architectView != null)
				architectView.OnPostCreate ();

			try
			{
                //var world = Intent.Extras.GetString(EXTRAS_KEY_ACTIVITY_ARCHITECT_WORLD_URL);

                //architectView.Load(world);
                architectView.Load(SAMPLE_WORLD_URL);

            }
			catch (Exception ex)
			{
				Log.Error ("WIKITUDE_SAMPLE", ex.ToString ());
			}
		}
        /*      static public int getRotation(Activity activity)
              {
                  int result = 1;
                  try
                  {
                      Display display = ((Windowmanager)activity.GetSystemService(Context.WindowService)).getDefaultDisplay();
                      Java.Lang.Object retObj = mDefaultDisplay_getRotation.invoke(display);
                      if (retObj != null)
                      {
                          result = (Integer)retObj;
                      }
                  }
                  catch (Exception ex)
                  {
                      //ex.printStackTrace();
                  }
                  return result;
              }
              */

        protected float[] lowPass(float[] input, float[] output)
        {
            float ALPHA = 0.25f;
            if (output == null) return input;

            for (int i = 0; i < input.Length; i++)
            {
                output[i] = output[i] + ALPHA * (input[i] - output[i]);
            }
            return output;
        }
        protected void LoadData()
        {
            if (isLoading)
                return;

            Task.Factory.StartNew(() =>
            {

                isLoading = true;

                while (lastKnownLocation == null && !this.IsFinishing)
                    Thread.Sleep(2000);

                if (this.lastKnownLocation != null && !this.IsFinishing)
                {

                    poiData = GeoUtils.GetPoiInformation(lastKnownLocation, mAzimuth,inclination);
                    Console.WriteLine("aaaaaaaaaaaaaaa");
                    Console.WriteLine(poiData.ToString() + "zzzzzzzzzzzzzzzzzz");
                    Console.WriteLine("bbbbbbbbbbbbbbbbb");
                    var js = "World.loadPoisFromJsonData(" + poiData.ToString() + ");";

                    architectView.CallJavascript(js);
                }

                isLoading = false;

            }).ContinueWith(t =>
            {

                isLoading = false;

                var ex = t.Exception;
                Log.Error(Constants.LOG_TAG, ex.ToString());

            }, TaskContinuationOptions.OnlyOnFaulted);
        }

        private void clearRotHist()
        {
            
            mRotHist.Clear();
            mRotHistIndex = 0;
        }

        private void setRotHist()
        {
            
            float[] hist = rMat;
            if (mRotHist.Count() == mHistoryMaxLength)
            {
                mRotHist.RemoveAt(mRotHistIndex);
            }
            mRotHist.Insert(mRotHistIndex++, hist);
            mRotHistIndex %= mHistoryMaxLength;
        }

        private float findFacing()
        {
            
            float[] averageRotHist = average(mRotHist);
            return (float)Math.Atan2(-averageRotHist[2], -averageRotHist[5]);
        }

        public float[] average(List<float[]> values)
        {
            float[] result = new float[9];
            foreach (float[] value in values)
            {
                for (int i = 0; i < 9; i++)
                {
                    result[i] += value[i];
                }
            }

            for (int i = 0; i < 9; i++)
            {
                result[i] = result[i] / values.Count();
            }

            return result;
        }

    }
}

